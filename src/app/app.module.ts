//import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_BASE_HREF, Location } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
//https://github.com/angular/flex-layout/wiki


import { AppComponent } from './app.component';


//import { SimpleChartModule } from './simple-chart/simple-chart.module'

//import { NgxChartsModule } from '@swimlane/ngx-charts'
//import { NgxUIModule } from '@swimlane/ngx-ui';

// Interface TimeSerie Chart:

import { D3_TimeserieChartModule } from './charts/d3-timeserie/d3-timeserie.module'
import { ColorPickerModule } from 'angular4-color-picker';

import { WorkerAppModule } from '@angular/platform-webworker';

@NgModule({
    providers: [
        {
            provide: APP_BASE_HREF,
            useFactory: getBaseLocation
        }
    ],
    declarations: [
        AppComponent,
    ],
    imports: [
        CommonModule,
        WorkerAppModule,
        FlexLayoutModule,
        ColorPickerModule,
        FormsModule,
        HttpModule,
        D3_TimeserieChartModule,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }


export function getBaseLocation() {
    console.log("getBaseLocation: ", location.pathname)
    const paths: string[] = location.pathname.split('/').splice(1, 1);
    const basePath: string = (paths && paths[0]) || '';
    return '/' + basePath;
}


