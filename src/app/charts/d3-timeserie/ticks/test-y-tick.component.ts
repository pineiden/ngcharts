import {
    Component, OnInit,
    ViewEncapsulation,
    OnChanges, SimpleChanges,
    ChangeDetectionStrategy,
} from '@angular/core';
import { Location, LocationStrategy, HashLocationStrategy } from '@angular/common';



@Component({
    selector: 'test-y-tick',
    providers: [Location, { provide: LocationStrategy, useClass: HashLocationStrategy }],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './test-y-tick.component.html',
    styleUrls: ['./test-y-tick.component.scss']
})

export class TestYTickComponent implements OnInit, OnChanges {
    width: number = 600;
    height: number = 600;
    label = "Test Y Tick";
    y_position = 250;
    font_size_label = 14
    axis_x = {}
    axis_y = {}
    central_y_position: number;
    left_x_val = 0;
    right_x_val = 0;
    x1 = 0;
    x2 = 0;

    tick_grid = {}
    tick_line = {}

    pos_text: {
    };

    tick_length: number = 30;

    padd_w: number = 50;
    padd_h: number = 50;


    ngOnInit() {
        console.log("Test tick y axis")
        console.log("Test y label", this.label)
        this.setYaxis()
        this.setXaxis()
        this.set_x_position()
        this.getTickSize()
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.set_x_position()
        this.getTickSize()
    }


    setYaxis() {
        this.axis_y = {
            x1: 20,
            y1: 20,
            x2: 20,
            y2: this.height - 100
        }
    }

    setXaxis() {
        this.axis_x = {
            x1: 20,
            y1: this.axis_y['y2'] / 2 + 20,
            x2: this.width - 20,
            y2: this.axis_y['y2'] / 2 + 20
        }
    }

    set_x_position() {
        this.x1 = this.axis_x["x1"]
        this.x2 = this.axis_x["x2"]
    }


    getTickSize() {
        console.log("new tick, resize")
        console.log(this.x1, this.x2)
        // tick grid
        this.tick_grid = {
            x1: this.x1,
            y1: this.y_position,
            x2: this.x2,
            y2: this.y_position
        }
        // tick line
        this.tick_line = {
            x1: this.x1 - this.tick_length,
            y1: this.y_position,
            x2: this.x2,
            y2: this.y_position
        }
        this.pos_text = {
            x: this.tick_line["x1"],
            y: this.y_position,
        }

        //this.get_left_x()
        //this.get_rigth_x()
    }

    font_size() {
        return this.font_size_label
    }

    minus_font() {
        this.font_size_label = this.font_size_label - 1
    }

    plus_font() {
        this.font_size_label = this.font_size_label + 1
    }

    get_up_y() {
        this.y_position = this.y_position - 1
    }

    get_down_y() {
        this.y_position = this.y_position + 1
    }

    left_x() {
        this.x1 = this.x1 - 1
        this.x2 = this.x2 - 1
    }

    right_x() {
        this.x1 = this.x1 + 1
        this.x2 = this.x2 + 1
    }

    yTickTransform() {
        return "translate(" + this.padd_w + "," + this.padd_h + ")"
    }

    control_yTickTransform() {
        return "translate(" + this.x1 + ",0)"
    }

}
