import {
    Component,
    Input,
    Output,
    EventEmitter,
    ViewEncapsulation,
    HostListener,
    ElementRef,
    ChangeDetectorRef,
    ChangeDetectionStrategy,
    ContentChild,
    TemplateRef,
    OnInit,
    NgZone,
    AfterViewInit,
    OnDestroy,
    OnChanges,
    SimpleChanges
} from '@angular/core';
import {
    trigger,
    state,
    style,
    animate,
    transition
} from '@angular/animations';

import { LocationStrategy } from '@angular/common';
import { PathLocationStrategy } from '@angular/common';
import { scaleLinear, scaleTime, scalePoint, scaleUtc } from 'd3-scale';
import { axisBottom, axisLeft } from 'd3-axis';
import { select } from 'd3-selection';
import { timeMinute } from 'd3-time'
import { curveLinear } from 'd3-shape';
import { range } from 'd3-array'

//import { calculateViewDimensions, ViewDimensions } from '@swimlane/ngx-charts';
//import { ColorHelper } from '@swimlane/ngx-charts';
//import { BaseChartComponent } from '@swimlane/ngx-charts';
//import { id } from '@swimlane/ngx-charts/release/utils/id';

import { trimLabel } from './trim-label.helper';
import { reduceTicks } from './ticks.helper';

import { Observable } from 'rxjs';
//import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/do';


@Component({
    selector: 'd3-timeserie',
    templateUrl: 'templates/d3-timeserie.component.html',
    styleUrls: ['./d3-timeserie.styles.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        /*
                trigger('animationState', [
                    transition(':leave', [
                        style({
                            opacity: 1,
                        }),
                        animate(500, style({
                            opacity: 0
                        }))
                    ])
                ])*/
    ]

})
export class D3TimeSerieComponent implements OnInit,
    OnChanges,
    AfterViewInit,
    OnDestroy {

    timenow = new Date();


    @Input() chart_settings: {};
    /*
      Structure:
      {
      mode:{
      chart: 'static'||'real_time'
      movement:OR {'animate', 'full','acummulate','dynamic','dependant'}
      update: {
      'animate': time numeric
      'full': ignored
      'acummulate': ignored (receive all elements)
      'dynamic': numeric (time to limit, from now to dtime in seconds to the past)
      'dependant':'for every input, add new data and delete the first, the value is numeric, amount of elements'

      }
      }


      */


    @Input() results: any;
    @Input() view: number[];
    @Input() var_name: string;


    width: number;
    height: number

    svg_width: number;
    svg_height: number;

    axis_x = {}
    axis_x_bottom = {}
    axis_y = {}

    //  rect properties

    rect_w: number;
    rect_h: number;
    rect_x: number;
    rect_y: number;
    rect_styles: string;

    draw: boolean = true;

    padding_h: number = 60;
    padding_w: number = 50;

    // series domain

    seriesDomain: any;


    // axis labels
    // options
    showXAxis = true;
    showYAxis = true;
    gradient = false;
    showLegend = true;
    showTicks = false;

    // x axis def
    xSet: any;
    xTimeScale = scaleUtc()
    showGridLines: boolean = true;
    showXAxisLabel: boolean = true;

    @Input() xAxisTickFormatting: any;
    @Input() tickFormatting;
    @Input() tickArguments = [5];

    @Input() labelText: string = "DateTime";

    xDomain: any;
    yDomain: any;
    yScale: any;
    xScale: any;

    scaleType: string = 'time';
    // Tick on x axis
    stroke: string = 'stroke';
    tickStroke: string = '#ccc';
    strokeWidth: string = 'none';
    @Input() xOrient: string = 'bottom';
    scale: any;
    verticalSpacing: number = 20;
    tickFormat: (o: any) => any;
    textTransform: any;
    textAnchor: string = 'middle';
    filteredDomain: any;
    adjustedScale: any;

    tickValues: any;
    ticks: any;

    x_tick_list: any[] = [];


    // Y axis def
    yScaleDict: any;
    y_tick_list: any[] = [];
    // set grids



    /*
      Load all modules and services when start

    */

    /*
      Series to obtain limits

    */
    max_list: number[];
    min_list: number[];

    y1: number = 0;
    y2: number = 0;


    svg_select: any;

    //TITLEs axis and chart

    @Input() set_titles: any;

    x_title_anchor = "start"
    y_title_anchor = "start"
    timeserie_title_anchor = "middle"

    // Basic shape
    @Input() figure_type: string;
    @Input() properties: any;
    @Input() position_x_y: any;
    position_XY: any;

    @Output() shape_position_emitter = new EventEmitter();
    shape_position = {}

    datetime_list: any[] = []
    tdomain: any[] = []
    rescale: any
    yLimit: number = 0
    x_range: any = {}

    status_init = 0;


    @Input() shape_position_counter: number;


    // manage list of shapes, when in some time (new time) input a new figure

    shape_list: any[] = [];
    show_shapes: boolean = false;

    /*
@Input() set shape_data(shape: {}) {
    console.log("Activando input setter")
    this.add_new_shape(shape)
    }*/
    @Input() shape_data: {};
    @Input() new_figure_type: string;
    @Input() new_properties: {};
    //@Input() new_position_XY: {};

    /*
on changes when the component has a new shape, run ngOnChanges
shape data must have this structure:
{datetime, value, min, max, error} ,
where datetime <-- x and value <-- y
    */

    //
    drop_list: any[] = []

    //scales






    constructor(
        protected chartElement: ElementRef,
        protected zone: NgZone,
        protected cd: ChangeDetectorRef,
        protected location: LocationStrategy
    ) {

        this.scale = this.xTimeScale

        // move to update

        this.adjustedScale = this.scale.bandwidth ? function(d) {
            return this.scale(d) + this.scale.bandwidth() * 0.5;
        } : this.scale;
        //this.verticalSpacing = 10;

        console.log("Trimlabel", trimLabel)

        this.svg_select = select('#d3ts-canvas')

        this.x_tick_list.push()

        console.log("Set titles", this.set_titles)

    }
    /*
      Data structure:
      name:
      series:[{values, min_, max_, datetime}]
    
     */


    setYaxis() {
        this.axis_y = {
            x1: 0,
            y1: 0,
            x2: 0,
            y2: this.height
        }
    }

    setXaxis() {
        this.axis_x = {
            x1: 0,
            y1: (this.axis_y['y2'] - this.axis_y['y1']) / 2,
            x2: this.width,
            y2: (this.axis_y['y2'] - this.axis_y['y1']) / 2
        }
    }

    setXaxisBottom() {
        this.axis_x_bottom = {
            x1: 0,
            y1: this.axis_y['y2'],
            x2: this.width,
            y2: this.axis_y['y2']
        }
    }

    /*
      When initialize run some main scripts and functionalities

     */
    ngOnInit() {
        this.position_XY = {
            x: 0,
            y: 0
        }

        console.log("Set titles", this.set_titles)
        console.log("On init ok")
        this.width = this.view[0]
        this.height = this.view[1]
        this.svg_width = this.width + 3 * this.padding_w
        this.svg_height = this.height + 2 * this.padding_h

        // set rect
        //this.seriesDomain = this.getSeriesDomain();
        this.setYaxis()
        this.setXaxis()
        this.setXaxisBottom()
        console.log("Axis x", this.axis_x)

        //  when update a new value
        // add to results.series the new data {value, min, max, error}
        this.getSeriesMax()
        this.getSeriesMin()

        this.set_y_position()
        // scale axis x
        // get domain
        var datetime_list = this.getSeriesDomain()
        var domain = this.getXDomain(datetime_list)
        var x_width = this.getXwidth()
        var xScaleDict = this.getXScale(domain, x_width)
        this.xScale = xScaleDict.scale
        var range = xScaleDict.range
        console.log("Range:", range)
        var range_st = range(this.axis_x["x1"], this.axis_x["x2"], x_width / 5)
        var tick_list = this.xScale.ticks(5)//timeMinute.every(2))
        console.log("Tick list")
        console.log(tick_list)
        console.log(this.xScale)
        let xaxis = axisBottom(this.xScale).ticks(5)
        console.log("X axis")
        console.log("Iteracion")
        console.log(typeof (tick_list))
        this.xScale = range
        tick_list.forEach(d => {
            console.log("Iteracion, este tick")
            console.log(d)
            console.log(range(d))
            var tick: any = {};
            tick['label'] = d
            tick['x_position'] = range(d)
            console.log(typeof (this.x_tick_list))
            console.log("New tic: ", tick)

            this.x_tick_list.push(tick)


        })
        console.log(this.x_tick_list)
        this.showTicks = true;
        console.log(xaxis)
        console.log(this.xScale.bandwidth)

        var last_tick = this.x_tick_list[this.x_tick_list.length - 1]
        var now_date = new Date(last_tick.label)
        var x_name = now_date.toLocaleDateString()
        console.log("Fecha de ahora", x_name)

        this.set_titles.axis_x.label = this.set_titles.axis_x.label + "\n" + x_name
        console.log("SET_TITLE: ", this.set_titles)
        //tick_list.forEach(d => { console.log(d) })
        //select rect and put on buttom
        //console.log(this.svg_select)

        //this.svg_select.call(xaxis)

        // domaing, width <- axis_x

        //axis y
        var tdomain = this.getYScaleDomain()
        var yLimit = this.getYLimit(tdomain)
        this.yDomain = tdomain
        this.yScaleDict = this.getYScale(yLimit, this.height)

        this.yScale = this.yScaleDict.scale
        var range_y = this.yScaleDict.range
        console.log("Range y", range_y)

        let rescale = function(d, h, limit) { return -(h / (2 * limit)) * (d) + h / 2 }
        //this.yScale = rescale;
        var tick_list_y = this.yScale.ticks(6)//timeMinute.every(2))
        console.log("Tick list y")
        console.log(tick_list_y)
        console.log(this.yScale)
        tick_list_y.forEach(d => {
            console.log("Iteracion, este tick")
            console.log(d)
            console.log("This scale", range_y(d))
            console.log("Valor en canvas", rescale(d, this.height, yLimit))
            var tick: any = {};
            tick['label'] = d
            tick['y_position'] = rescale(d, this.height, yLimit)
            console.log(typeof (this.y_tick_list))
            console.log("Y New tic: ", tick)
            this.y_tick_list.push(tick)
        })

        console.log("X range", range)
        this.datetime_list = datetime_list
        this.tdomain = tdomain
        this.x_range = range
        this.rescale = rescale
        this.yLimit = yLimit
        this.getPositionShape()
    }

    getPositionShape() {
        let datetime_list = this.datetime_list;
        let tdomain = this.tdomain;
        let yLimit = this.yLimit;

        let x_index = Math.floor(Math.abs(Math.random() * datetime_list.length))
        let y_index = Math.floor(Math.abs(Math.random() * tdomain.length))

        this.position_XY = {
            x: this.x_range(datetime_list[x_index]),
            y: this.rescale(
                tdomain[y_index],
                this.height,
                yLimit)
        }

        this.shape_position = {
            index_x: x_index,
            index_y: y_index,
            chart_x: this.position_XY.x,
            chart_y: this.position_XY.y
        }

        this.shape_position_emitter.emit(this.shape_position)

    }

    /*
      After view init
     */
    ngAfterViewInit(): void {
        console.log("After Init ok")

        this.status_init = 1
    }


    /* When some variable change, run this*/
    ngOnChanges(changes: SimpleChanges): void {
        console.log("Hay un cambio")
        console.log(changes)
        console.log("Nuevo largo de results")
        console.log(this.results.series.length)
        console.log(this.results)



        console.log("New shape input", this.shape_data)
        console.log("Type of change", changes.name)
        var preresult = this.results
        if (this.status_init > 0) {
            let mode_chart = this.chart_settings['mode']['chart']
            let mode_movement = this.chart_settings['mode']['movement']
            let update_rate = this.chart_settings['update']
            if (mode_chart == 'real_time' && mode_movement == 'dynamic') {
                console.log("Dynamic chart:")
                //setTimeout(this.new_time.bind(this), 1000, update_rat
                //Observable.interval(5000).flatMapLatest(() =>)
                Observable.interval(5000).subscribe(x => {
                    // genera un ciclo cada 2000 milisegundos=2 segundo
                    this.new_time(update_rate)
                    // capturar las modificaciones
                    this.cd.markForCheck()
                });



            }
            for (let propName in changes) {
                let change = changes[propName];
                let curVal = JSON.stringify(change.currentValue);
                let prevVal = JSON.stringify(change.previousValue);
                console.log("Change name", propName)
                var this_change = {}
                this.drop_list = []
                if (propName == 'shape_data') {
                    console.log("Agregando nueva figura")
                    console.log(curVal)
                    //let pos_xy = this.shape_data['properties']
                    //var pxy = this.shape_data["position_XY"]

                    this_change = Object.assign(this_change, change.currentValue)

                    console.log("Properties: ", this_change["position_XY"])
                    console.log("This results before adding shape", this.results)

                    var position = this_change["position_XY"]

                    this.results.series.push(this_change["position_XY"])

                    console.log("This results after adding shape", this.results)
                    if (mode_chart == 'real_time' && mode_movement == 'dependant') {
                        if (this.results.series.length == update_rate) {
                            console.log("Borrando nuevos elementos")
                            console.log(this.results.series.length)
                            console.log(update_rate)
                            // because i will add 1 more data elements
                            // i have to drop the first
                            let q = this.results.series.splice(0, 1)
                            console.log("Elemento dropped:", q)
                            this.drop_list.push(q)
                            let q_datetime = q[0]['datetime']
                            // check if element (datetime) from q is in new shape
                            var del_list = []
                            this.shape_list.forEach((d, index) => {
                                console.log("Checking to drop:", d)
                                console.log("Data shape: ", d['position_XY']['datetime'])
                                let date_shape = new Date(d['position_XY']['datetime'])
                                let date_drop = new Date(q_datetime)
                                console.log("Shape", date_shape.getTime())
                                console.log("Drop", date_drop.getTime())
                                console.log("Check inequality: ", date_shape.getTime() <= date_drop.getTime())
                                if (date_shape.getTime() <= date_drop.getTime()) {
                                    console.log("Comparando datetimes:")
                                    console.log(q_datetime)
                                    console.log(d['datetime'])
                                    del_list.push(index)
                                }
                            })
                            console.log("A borrar:", del_list)
                            del_list.forEach(d => this.shape_list.splice(d, 1))
                        }
                    }
                    //console.log()
                    this.renew_axis()
                    this.shape_adjust(this_change, position)
                    //add new shape to list
                }
                else if (propName = "shape_position_counter") {
                    this.getPositionShape()
                }
                else {
                }
                console.log(curVal);
                console.log(prevVal);
            }

        }

    }

    /*When destroy some value*/
    ngOnDestroy(): void { }


    // creation canvas 

    defRectCanvas() {
        this.rect_h = this.height - 2 * this.padding_h
        this.rect_w = this.width - 2 * this.padding_w
        this.rect_x = this.padding_h
        this.rect_y = this.padding_w
    }

    // creation axes

    // ticks



    tickTransform(tick): string {
        return 'translate(' + this.adjustedScale(tick) + ',' + this.verticalSpacing + ')';
    }


    getSeriesDomain(): any[] {
        // obtain from series, the datetimes, using map
        return this.results.series.map(d => {

            var isodate: string = d.datetime;
            var value: number = new Date(isodate).getTime();
            return value
        });
    }

    //Get array of mins
    getSeriesMin(): any[] {
        return this.results.series.map(d => {
            var min_abs: number = Math.abs(d.min_);
            return min_abs
        });
    }


    getSeriesMax(): any[] {
        return this.results.series.map(d => {
            var max_abs: number = Math.abs(d.max_);
            return max_abs
        });
    }

    getYLimit(domain): number {
        var max_min = Math.min(...domain)
        var max_max = Math.max(...domain)


        return Math.max(max_max, max_min) * 1.1
    }


    // x axis scale
    getXScale(domain, width): any {
        var scale_;

        var range_;
        range_ = scaleTime().range([0, width])
        scale_ = range_.domain(domain);

        return { range: range_, scale: scale_ };
    }


    // y axis scale

    // Domaing among value, min, max from series range
    getYScaleDomain(): any[] {

        var set_list = new Set()
        this.results.series.map(d => {
            set_list.add(d.min)
        })
        this.results.series.map(d => {
            set_list.add(d.value)
        })
        this.results.series.map(d => {
            set_list.add(d.max)
        })

        if (set_list.size == 0) {
            set_list.add(1)
        }


        let domain_list = []

        set_list.forEach(v => domain_list.push(v))

        return domain_list
    }


    getYScale(yLimit, height): any {
        var scale_;
        var range_;
        console.log("Domain :", [-yLimit, yLimit])
        scale_ = scaleLinear().domain([-yLimit, yLimit])
        range_ = scale_.range([0, height])
        return { range: range_, scale: scale_ };
    }


    /* non sense*/
    updateDomain(domain): void {
        this.filteredDomain = domain;
        this.xDomain = this.filteredDomain;
        this.xTimeScale = this.getXScale(this.xDomain, this.width);
    }

    getXDomain(values): any[] {
        let domain = [];

        if (this.scaleType === 'time') {
            const min = Math.min(...values);
            const max = Math.max(...values);
            domain = [new Date(min), new Date(max)];
            this.xSet = [...values].sort((a, b) => {
                const aDate = a;
                const bDate = b;
                if (aDate > bDate) return 1;
                if (bDate > aDate) return -1;
                return 0;
            });
        } else if (this.scaleType === 'linear') {
            values = values.map(v => Number(v));
            const min = Math.min(...values);
            const max = Math.max(...values);
            domain = [min, max];
            this.xSet = [...values].sort();
        } else {
            domain = values;
            this.xSet = values;
        }

        return domain;
    }

    getTicks() {
        let ticks;
        const maxTicks = this.getMaxTicks(20);
        const maxScaleTicks = this.getMaxTicks(100);

        if (this.tickValues) {
            ticks = this.tickValues;
        } else if (this.scale.ticks) {
            ticks = this.scale.ticks.apply(this.scale, [maxScaleTicks]);
        } else {
            ticks = this.scale.domain();
            ticks = reduceTicks(ticks, maxTicks);
        }

        return ticks;
    }

    getMaxTicks(tickWidth: number): number {
        return Math.floor(this.width / tickWidth);
    }

    getXwidth() {
        var x_width = this.axis_x["x2"] - this.axis_x["x1"]
        return x_width
    }

    x_font_size() {
        return 12
    }

    y_font_size() {
        return 12
    }

    set_y_position() {
        this.y1 = this.axis_y["y1"]
        this.y2 = this.axis_y["y2"]
    }

    // main chart transform
    view_transform() {
        return "translate(" + this.padding_w + "," + this.padding_h + ")"
    }

    // x axis title
    x_title_transform() {
        return "translate(" + (this.padding_w + this.width) + "," + (this.axis_y["y2"] + (this.padding_h * 3 / 2)) + ")"
        //anchor end
    }
    x_title_font_size() {
        return this.set_titles.axis_x.font_size
    }
    x_axis_name() {
        return this.set_titles.axis_x.label
    }


    // y axis title
    y_title_transform() {
        return "translate(" + this.axis_x["x1"] + "," + (this.padding_h + ((this.axis_y["y2"] - this.axis_y["y1"]) / 2)) + ")"
        //anchor->start
    }
    y_title_font_size() {
        return this.set_titles.axis_y.font_size
    }
    y_axis_name() {
        return this.set_titles.axis_y.label
    }

    // chart title
    chart_title_transform() {
        return "translate(" + (this.padding_w + ((this.axis_x["x2"] - this.axis_x["x1"]) / 2)) + "," + (this.padding_h / 2) + ")"
        // anchor middle
    }
    timeserie_title_font_size() {
        return this.set_titles.chart.font_size

    }

    chart_name() {
        return this.set_titles.chart.label
    }

    is_figure() {
        return true
    }

    list_of_figures() {
        return this.show_shapes
    }

    add_new_shape(shape_data) {
        var ft = shape_data.figure_type
        var np = shape_data.properties
        var nxy = shape_data.position_XY
        var shape = {
            'figure_type': ft,
            'properties': np,
            'position_XY': nxy
        }
        this.shape_list.push(shape)
    }

    del_shape(id: number) {
        this.shape_list.slice(id, 1)
    }


    new_time(update_rate) {
        this.timenow = new Date()
        let timenow = new Date()
        console.log("New time settings:")
        console.log(this.chart_settings)

        let datetime_back = timenow.getTime() - update_rate * 1000
        var drop_data = []

        this.results.series.forEach((d, index) => {
            let dt = (new Date(d.datetime)).getTime()
            if (dt <= datetime_back) {
                drop_data.push(index)
            }
        })
        console.log("Dropped data", drop_data)

        drop_data.forEach(d => {
            let q = this.results.series.splice(d, 1)
            this.drop_external_shapes(q)
        })

        let new_data = { datetime: (new Date()).toISOString(), value: 0, min: 0, max: 0, error: 0 }
        this.results.series.push(new_data)
        // create  new ticks

        // axis x
        console.log("This series:", this.results.series)

        // axis y

        this.renew_axis()
        this.realocate_shapes()

    }

    renew_axis() {
        this.getSeriesMax()
        this.getSeriesMin()
        var datetime_list = this.getSeriesDomain()
        var domain = this.getXDomain(datetime_list)
        var x_width = this.getXwidth()
        var xScaleDict = this.getXScale(domain, x_width)
        this.xScale = xScaleDict.scale
        var x_range = xScaleDict.range
        var tick_list = this.xScale.ticks(5)//timeMinute.every(2))
        let xaxis = axisBottom(this.xScale).ticks(5)
        this.xScale = x_range
        this.x_tick_list = []
        tick_list.forEach(d => {
            //console.log("Iteracion, este tick")
            console.log(d)
            console.log(x_range(d))
            var tick: any = {};
            tick['label'] = d
            tick['x_position'] = x_range(d)
            //console.log(typeof (this.x_tick_list))
            //console.log("New tic: ", tick)
            this.x_tick_list.push(tick)

        })

        // update date
        var last_tick = this.x_tick_list[this.x_tick_list.length - 1]
        var now_date = new Date(last_tick.label)
        var x_name = now_date.toLocaleDateString()
        //console.log("Fecha de ahora", x_name)
        this.set_titles.axis_x.label = this.set_titles.axis_x.label + "\n" + x_name

        // update y ticks
        var tdomain = this.getYScaleDomain()
        var yLimit = this.getYLimit(tdomain)
        this.yDomain = tdomain
        //console.log("Obtaining scale in y")
        this.yScaleDict = this.getYScale(yLimit, this.height)

        this.yScale = this.yScaleDict.scale
        var range_y = this.yScaleDict.range
        //console.log("Range y", range_y)

        let rescale = function(d, h, limit) { return -(h / (2 * limit)) * (d) + h / 2 }
        //this.yScale = rescale;
        var tick_list_y = this.yScale.ticks(6)//timeMinute.every(2))
        //console.log("Tick list y")
        //console.log(tick_list_y)
        ///console.log(this.yScale)
        this.y_tick_list = []
        if (tick_list_y.length == 0) {
            tick_list_y = [-1, 1]
            yLimit = 1.5
        }
        tick_list_y.forEach(d => {
            // console.log("Iteracion, este tick")
            //console.log(d)
            //console.log("This scale", range_y(d))
            //console.log("Valor en canvas", rescale(d, this.height, yLimit))
            var tick: any = {};
            tick['label'] = d
            tick['y_position'] = rescale(d, this.height, yLimit)
            //console.log(typeof (this.y_tick_list))
            //console.log("Y New tic: ", tick)
            this.y_tick_list.push(tick)
        })

        this.x_range = x_range
        this.yLimit = yLimit
        this.rescale = rescale
    }


    shape_adjust(this_change, position) {
        let x_range = this.x_range
        let rescale = this.rescale
        let yLimit = this.yLimit

        // X and y axis adjust ok
        console.log("Position real", position)
        var shape_x = x_range(new Date(position.datetime))
        var shape_y = rescale(position.value, this.height, yLimit)
        var shape_y_max = rescale(position.max, this.height, yLimit)
        var shape_y_min = rescale(position.min, this.height, yLimit)
        var addpos = Object.assign(this_change["position_XY"], {
            x: shape_x,
            y: shape_y,
            gmin: shape_y_min,
            gmax: shape_y_max
        })
        console.log("New shape with changes", addpos)
        //add addpos to shape
        this_change["position_XY"] = addpos
        console.log("New shape with changes", this_change)
        //recalculate position on shape_list
        this.realocate_shapes()
        this.shape_list.push(this_change)
        console.log("Shape list: ", this.shape_list)
        if (this.shape_list) {
            this.show_shapes = true
        }
    }

    drop_external_shapes(q) {
        console.log("Elemento dropped:", q)
        this.drop_list.push(q)
        let q_datetime = q[0]['datetime']
        // check if element (datetime) from q is in new shape
        var del_list = []
        this.shape_list.map((d, index) => {
            console.log("Checking to drop:", d)
            console.log("Data shape: ", d['position_XY']['datetime'])
            let date_shape = new Date(d['position_XY']['datetime'])
            let date_drop = new Date(q_datetime)
            console.log("Shape", date_shape.getTime())
            console.log("Drop", date_drop.getTime())
            console.log("Check inequality: ", date_shape.getTime() <= date_drop.getTime())
            if (date_shape.getTime() <= date_drop.getTime()) {
                console.log("Comparando datetimes:")
                console.log(q_datetime)
                console.log(d['datetime'])
                del_list.push(index)
            }
        })
        console.log("A borrar:", del_list)
        del_list.forEach(d => this.shape_list.splice(d, 1))

    }

    realocate_shapes() {
        let x_range = this.x_range
        let rescale = this.rescale
        let yLimit = this.yLimit


        this.shape_list.map(d => {
            var position = d.position_XY
            var shape_x = x_range(new Date(position.datetime))
            var shape_y = rescale(position.value, this.height, yLimit)
            var shape_y_max = rescale(position.max, this.height, yLimit)
            var shape_y_min = rescale(position.min, this.height, yLimit)
            position.x = shape_x;
            position.y = shape_y;
            position.gmin = shape_y_min
            position.gmax = shape_y_max
            Object.assign(d.position_XY, position)
        })
    }


}

