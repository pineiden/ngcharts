import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// simple-timeserie components:
import { D3TimeSerieComponent } from './d3-timeserie.component'
// Axis components
// tick

import { XAxisTickComponent } from './ticks/x_tick.component'
import { TestXTickComponent } from './ticks/test-x-tick.component'

import { YAxisTickComponent } from './ticks/y_tick.component'
import { TestYTickComponent } from './ticks/test-y-tick.component'

// ngx-charts
//import { NgxChartsModule } from '@swimlane/ngx-charts'

import { D3TimeSerieRoutingModule } from './d3-timeserie.routing.module'

import { RouterModule, Routes } from '@angular/router';

import { BasicShapeComponent } from './elements/shapes/basic.component'


@NgModule({
    imports: [
        //NgxChartsModule,
        CommonModule,
        D3TimeSerieRoutingModule],
    declarations: [
        //components
        D3TimeSerieComponent,
        XAxisTickComponent,
        TestXTickComponent,
        YAxisTickComponent,
        TestYTickComponent,
        BasicShapeComponent
    ],
    exports: [
        //components
        D3TimeSerieComponent,
        XAxisTickComponent,
        TestXTickComponent,
        YAxisTickComponent,
        TestYTickComponent,
        BasicShapeComponent,
        RouterModule
    ]
})
export class D3_TimeserieChartModule { }
