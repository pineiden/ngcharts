import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnChanges,
    OnInit,
    ElementRef,
    ViewChild,
    SimpleChanges,
    AfterViewInit,
    ChangeDetectionStrategy
} from '@angular/core';

import {
    trigger,
    state,
    style,
    animate,
    transition
} from '@angular/animations';


@Component({
    selector: 'g[basic-shape]',
    templateUrl: './basic.component.html',
    styleUrls: ['./basic.styles.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        /*        trigger('animationState', [
                    transition(':leave', [
                        style({
                            opacity: 1,
                        }),
                        animate(500, style({
                            opacity: 0
                        }))
                    ])
                ])*/
    ]
})

export class BasicShapeComponent implements OnInit, OnChanges, AfterViewInit {

    // viewchild

    @ViewChild('svg:g')
    private svg: ElementRef;

    //

    @Input() figure_type: string;
    @Input() properties: any;
    @Input() position_x_y: any;
    @Input() xScale: any;
    @Input() yScale: any;

    ngOnInit() {

    }

    ngOnChanges() {

    }

    ngAfterViewInit() {

    }

    shapeStyle() {
        var shape_style = {
            fill: this.properties["fill"],
            stroke: this.properties["stroke"],
            "stroke-width": this.properties["stroke_width"]
        }
        console.log("Shape Style: ", shape_style)
        return shape_style
    }

    shapeTransform() {
        if (this.position_x_y) {
            return ` translate(${this.position_x_y.x},${this.position_x_y.y})`
        }
    }

    // manage list of figures

}
