import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormBuilder } from '@angular/forms';  // <-- #1 import module
import { FormGroup } from '@angular/forms';  // <-- #1 import module
import { Validators } from '@angular/forms';

import {
    Input,
    Output,
    EventEmitter,
    OnInit,
} from '@angular/core';

import {
    trigger,
    state,
    style,
    animate,
    transition
} from '@angular/animations';


@Component({
    selector: 'select-shape',
    templateUrl: './select_shape.component.html',
    styleUrls: ['./select_shape.styles.scss'],
    animations: [
        /*        trigger('animationState', [
                    transition(':leave', [
                        style({
                            opacity: 1,
                        }),
                        animate(500, style({
                            opacity: 0
                        }))
                    ])
                ])*/
    ]
})

class SelectShapeComponent implements OnInit {

    shape_form: FormGroup;

    constructor(private fb: FormBuilder) {
        this.createForm()

    }

    ngOnInit() {

    }

    createForm() {
        this.shape_form = this.fb.group(
            {
                name: '',
                position_x: '',
                position_y: '',
            }
        )
    }

}
