import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Location, LocationStrategy, HashLocationStrategy } from '@angular/common';
import * as shape from 'd3-shape';
import * as d3 from 'd3';

import { GPS } from "./utils/gps"
import { gps_list } from './utils/data'
// source of data

import { D3TimeSerieComponent } from './charts/d3-timeserie/d3-timeserie.component'

///

import { ColorPickerService } from 'angular4-color-picker';


const monthName = new Intl.DateTimeFormat('en-us', { month: 'short' });
const weekdayName = new Intl.DateTimeFormat('en-us', { weekday: 'short' });

@Component({
    selector: 'app-root',
    providers: [Location, { provide: LocationStrategy, useClass: HashLocationStrategy }],
    encapsulation: ViewEncapsulation.None,
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    //directives: [D3TimeSerieComponent]
})
export class AppComponent implements OnInit {

    showD3Timeserie: boolean = false

    gps_data: GPS[];

    amount_data: number = 10;
    data_serie: {};
    title = 'App';
    chart_name: string = 'time-serie';
    chart: any;
    chartType: string
    view: any[];
    width: number = 800;
    height: number = 300;
    data: {};
    countries: any[];
    multi: any[];
    realTimeData: boolean = true;
    tooltipDisabled = true;
    timeline = false;
    gps_axis = 'N'
    count = 0;

    linearScale: boolean = false;
    fitContainer: boolean = false;
    // options
    showXAxis = true;
    showYAxis = true;
    gradient = false;
    showLegend = true;
    legendTitle = 'Legend';
    showXAxisLabel = true;
    xAxisLabel = 'Time';
    showYAxisLabel = true;
    yAxisLabel = 'N [m]';
    showGridLines = true;
    colorScheme = {
        domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
    };
    schemeType: string = 'ordinal';
    tohtml: string = ''

    // line, area
    autoScale = true;

    set_titles = {
        axis_x: { label: "Time", font_size: 18 },
        axis_y: { label: "N", font_size: 18 },
        chart: { label: "GPS North TEST-CSN", font_size: 22 }
    }

    chartGroups = [
        {
            name: 'Time Serie Chart',
            charts: [
                {
                    name: 'Time Serie Chart',
                    selector: 'time-serie',
                    options: [
                        'colorScheme', 'schemeType', 'showXAxis', 'showYAxis', 'gradient',
                        'showLegend', 'legendTitle', 'showXAxisLabel', 'xAxisLabel', 'showYAxisLabel',
                        'yAxisLabel', 'autoScale', 'timeline', 'showGridLines', 'curve',
                        'rangeFillOpacity', 'roundDomains', 'tooltipDisabled', 'showRefLines',
                        'referenceLines', 'showRefLabels'
                    ],
                    defaults: {
                        yAxisLabel: this.yAxisLabel,
                        xAxisLabel: this.xAxisLabel,
                        linearScale: true
                    }
                }
            ]
        }
    ]

    curves = {
        Basis: shape.curveBasis,
        'Basis Closed': shape.curveBasisClosed,
        Bundle: shape.curveBundle.beta(1),
        Cardinal: shape.curveCardinal,
        'Cardinal Closed': shape.curveCardinalClosed,
        'Catmull Rom': shape.curveCatmullRom,
        'Catmull Rom Closed': shape.curveCatmullRomClosed,
        Linear: shape.curveLinear,
        'Linear Closed': shape.curveLinearClosed,
        'Monotone X': shape.curveMonotoneX,
        'Monotone Y': shape.curveMonotoneY,
        Natural: shape.curveNatural,
        Step: shape.curveStep,
        'Step After': shape.curveStepAfter,
        'Step Before': shape.curveStepBefore,
        default: shape.curveLinear
    };
    curveType: string = 'Linear';
    curve: any = this.curves[this.curveType];


    // select figure type:
    //Set on plots:

    figure_type: string = "circle"

    properties = {
        cx: 0,
        cy: 0,
        r: 20,
        fill: "red",
        stroke: "gray",
        stroke_width: 3,
    }
    position_x_y = {
        x: 100,
        y: 200,
    }

    shape_position = {
        index_x: 0,
        index_y: 0,
        chart_x: 0,
        chart_y: 0,
    }

    new_shape_position = 0;

    // adding new shape
    select_figures = [
        { value: 'circle', name: 'Círculo' },
        { value: 'square', name: 'Cuadrado' }]
    selected_figure: string;
    new_shape_figure: string;
    new_shape = { radius: 5, side_length: 5 / Math.sqrt(2) }
    shape_list: any[] = [];
    send_figure: {};


    //figure_type

    //new y

    // properties

    //datetime shape

    // chart settigns

    chart_settings = {
        'mode': { 'chart': 'real_time', 'movement': 'dynamic' },
        'update': 60
    }

    // color and styles properties on shapes

    color_fill: string = 'rgba(200,100,50,0.7)'
    color_stroke: string = '#1f1'
    borders_size: number[] = [1, 2, 3, 4, 5, 6]
    border_size = 0


    constructor(
        public location: Location,
        private cpService: ColorPickerService) {
        this.view = [this.width, this.height]
        this.gps_data = gps_list(this.amount_data)

        // load data to principal component
        console.log("Constructor")
        //Object.assign(this, { tree_example })
        //this.data = tree_example
        //Object.assign(this, { multi })

    }

    ngOnInit() {
        this.chartType = 'time-serie'
        this.realTimeData = true
        console.log(this.gps_data)

        console.log(this.data_serie)
        this.data_serie = this.gps2ds(this.gps_data)
        console.log(this.data_serie)

        //setInterval(this.updateData.bind(this), 1000)

        if (!this.fitContainer) {
            this.applyDimensions();
        }

        this.tohtml = JSON.stringify(this.data_serie)
    }

    /*
      It's a function to test the way in that the GPS data is received

     */

    applyDimensions() {
        this.view = [this.width, this.height];
    }


    getFlag(country) {
        return this.countries.find(c => c.name === country).emoji;
    }

    selectChart(chartSelector) {
        this.chartType = chartSelector = chartSelector.replace('/', '');
        this.location.replaceState(this.chartType);

        for (const group of this.chartGroups) {
            this.chart = group.charts.find(x => x.selector === chartSelector);
            if (this.chart) break;
        }

        this.linearScale = false;
        //this.yAxisLabel = 'N';
        //this.xAxisLabel = 'Time';

        this.width = 700;
        this.height = 200;

        Object.assign(this, this.chart.defaults);

        if (!this.fitContainer) {
            this.applyDimensions();
        }
    }

    yLeftAxisScale(min, max) {
        return { min: `${min}`, max: `${max}` };
    }

    yRightAxisScale(min, max) {
        return { min: `${min}`, max: `${max}` };
    }

    yLeftTickFormat(data) {
        return `${data.toLocaleString()}`;
    }

    yRightTickFormat(data) {
        return `${data}%`;
    }

    gps2ds(gps) {
        var dataserie = {
            N: { name: gps[0].name, series: [] },
            E: { name: gps[0].name, series: [] },
            U: { name: gps[0].name, series: [] }
        }
        gps.forEach(d => {
            dataserie.N.series.push(d.N)
            dataserie.E.series.push(d.E)
            dataserie.U.series.push(d.U)

        })
        return dataserie

    }

    getCalendarData(): any[] {
        // today
        const now = new Date();
        const todaysDay = now.getDate();
        const thisDay = new Date(now.getFullYear(), now.getMonth(), todaysDay);

        // Monday
        const thisMonday = new Date(thisDay.getFullYear(), thisDay.getMonth(), todaysDay - thisDay.getDay() + 1);
        const thisMondayDay = thisMonday.getDate();
        const thisMondayYear = thisMonday.getFullYear();
        const thisMondayMonth = thisMonday.getMonth();

        // 52 weeks before monday
        const calendarData = [];
        const getDate = d => new Date(thisMondayYear, thisMondayMonth, d);
        for (let week = -52; week <= 0; week++) {
            const mondayDay = thisMondayDay + (week * 7);
            const monday = getDate(mondayDay);

            // one week
            const series = [];
            for (let dayOfWeek = 7; dayOfWeek > 0; dayOfWeek--) {
                const date = getDate(mondayDay - 1 + dayOfWeek);

                // skip future dates
                if (date > now) {
                    continue;
                }

                // value
                const value = (dayOfWeek < 6) ? (date.getMonth() + 1) : 0;

                series.push({
                    date,
                    name: weekdayName.format(date),
                    value
                });
            }

            calendarData.push({
                name: monday.toString(),
                series
            });
        }

        return calendarData;
    }


    calendarAxisTickFormatting(mondayString: string) {
        const monday = new Date(mondayString);
        const month = monday.getMonth();
        const day = monday.getDate();
        const year = monday.getFullYear();
        const lastSunday = new Date(year, month, day - 1);
        const nextSunday = new Date(year, month, day + 6);
        return (lastSunday.getMonth() !== nextSunday.getMonth()) ? monthName.format(nextSunday) : '';
    }

    calendarTooltipText(c): string {
        return `
      <span class="tooltip-label">${c.label} • ${c.cell.date.toLocaleDateString()}</span>
      <span class="tooltip-val">${c.data.toLocaleString()}</span>
    `;
    }


    onLegendLabelClick(entry) {
        console.log('Legend clicked', entry);
    }

    dateDataWithOrWithoutRange() {
        this.count += 1
        console.log("Count ", this.count)
        console.log("Entregando a chart")
        console.log(this.data_serie['N'])
        console.log("Series")
        console.log(this.data_serie['N'].series)

        return this.data_serie['N']
    }


    shapePositionUpdated($event) {
        console.log("Se registra posicion de la figura en")
        console.log($event)
        this.shape_position = $event
    }

    new_position() {
        console.log("Creating new position and communitating with child component")
        this.new_shape_position++;

    }


    send_shape(form) {
        console.log("Enviando figura")
        console.log(this.new_shape)
        console.log(this.selected_figure)
        console.log(this.shape_list.indexOf(this.selected_figure))
        var figures: any[] = [];
        this.select_figures.forEach(d => { figures.push(d.value) })
        console.log(figures)
        if (figures.indexOf(this.selected_figure) >= 0) {
            var new_gps = gps_list(1)[0]
            console.log(new_gps)
            var position_XY = {
                datetime: new_gps.N["datetime"],
                value: new_gps.N["value"],
                min: new_gps.N["min"],
                max: new_gps.N["max"],
            }
            var figure = this.selected_figure;
            var attributes;
            var shape_styles = {
                fill: this.color_fill,
                stroke: this.color_stroke,
                stroke_width: this.border_size
            }

            if (this.selected_figure == 'circle') {
                attributes = {
                    cx: 0,
                    cy: 0,
                    r: this.new_shape.radius
                }

            }

            else if (this.selected_figure == 'square') {
                attributes = {
                    x1: 0,
                    y1: 0,
                    x2: this.new_shape.side_length,
                    y2: this.new_shape.side_length,
                }
            }

            Object.assign(attributes, shape_styles)

            var shape = {
                figure_type: figure,
                properties: attributes,
                position_XY: position_XY
            }
            console.log("Colocando", shape)

            this.send_figure = shape;
        }

    }

    showDiv(elem) {
        if (elem.value == 0) {
            document.getElementById('circle_form').style.display = "block";
        }
        else if (elem.value == 1) {
            document.getElementById('square_form').style.display = "block";
        }
    }

    selected_shape(shape) {
        console.log(this.selected_figure)
    }

    set_border_size() {
        console.log("Border size", this.border_size)
    }


}
