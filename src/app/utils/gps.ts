export class GPS {
    name: string;
    TS: number;
    N: {
        name: string,
        value: number,
        error: number,
        max: number,
        min: number
    };
    E: {
        name: string,
        value: number,
        error: number,
        max: number,
        min: number
    };
    U: {
        name: string,
        value: number,
        error: number,
        max: number,
        min: number
    }

    constructor(GPS_: any) {
        this.name = GPS_.name;
        this.TS = GPS_.TS;
        this.N = GPS_.N;
        this.E = GPS_.E;
        this.U = GPS_.U;
    }
}
