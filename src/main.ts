import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import { bootstrapWorkerUi } from '@angular/platform-webworker';


/*if (process.env.ENV === 'production') {
    enableProdMode();
    }*/

if (environment.production) {
    enableProdMode();
}

//platformBrowserDynamic().bootstrapModule(AppModule);

bootstrapWorkerUi('webworker.bundle.js');
